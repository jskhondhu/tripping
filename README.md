# tripping
Trippin' with WP

```
apt-get install tripwire -y
/etc/tripwire
vi twpol.txt
twadmin -m P twpol.txt 
//baseline
tripwire --init
//check
tripwire --check
```
```
# Ruleset for Wordpress
(
  rulename = "Wordpress Ruleset",
  severity= $(SIG_HI)
)
{
        /var/www        -> $(SEC_CRIT);
}
```

```
gem install wpscan
ruby wpscan.rb --update
ruby wpscan.rb --url http://yourwebsite.com
//vuln plugins
ruby wpscan.rb --url http://yourwebsite.com --enumerate vp
//vuln themes
ruby wpscan.rb --url http://yourwebsite.com --enumerate vt
//user enum
ruby wpscan.rb --url http://yourwebsite.com --enumerate u
//pass dict atk
ruby wpscan.rb --url http://yourwebsite.com --wordlist passwords.txt threads 50
```
